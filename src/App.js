import React from 'react';

import './App.scss';
import Home from './components/Home/Home';
import { ReactComponent as Logo } from "./assets/30-logo_nask_white.svg";

function App() {
  return (
    <div className="app">
      <div className="app__blue-block">
        <Logo className="app__logo" />
      </div>
      <Home />
      <div id="notification" >
        <div className="notification">
          <div className="notification__overlay"></div>
          <div className="notification__box">
            <div>
              <p className="notification__heading">Wystąpił bląd</p>
              <p className="notification__message">Nie udało się wykonać żądanej operacji, ponieważ nie znaleziono zasobu powiązanego z żądaniem.</p>
            </div>
            <svg className="notification__close" id="close-notification" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M6 18L18 6M6 6l12 12"></path></svg>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
