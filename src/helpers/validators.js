export const required = value => (value ? undefined : 'To pole jest wymagane')
export const mustBeNumber = value => (isNaN(value) ? 'Wartość musi być liczbą' : undefined)
export const minValue = min => value =>
    isNaN(value) || value >= min ? undefined : `Wartość musi być większa od ${min}`
export const composeValidators = (...validators) => value =>
    validators.reduce((error, validator) => error || validator(value), undefined)