import React, { useReducer, useEffect } from "react";

const LOCACL_STORAGE_KEY = "CurrencyConverter.History";

const reducer = (conversionHistoryState = [], action) => {
    switch (action.type) {
        case 'add':
            return [action.newConversion, ...conversionHistoryState];
        case 'clear':
            conversionHistoryState = [];
            break;
        default:
            throw new Error("History error");
    }
};

const localState = JSON.parse(localStorage.getItem(LOCACL_STORAGE_KEY));

const HistoryContext = React.createContext();

function HistoryProvider(props) {
    const [conversions, setConversion] = useReducer(reducer, localState || []);

    useEffect(() => {

        if (conversions) {
            localStorage.setItem(LOCACL_STORAGE_KEY, JSON.stringify(conversions));
        } else {
            localStorage.removeItem(LOCACL_STORAGE_KEY)
        }

    }, [conversions]);

    return (
        <HistoryContext.Provider value={{ conversions, setConversion }}>
            {props.children}
        </HistoryContext.Provider>
    );
}

export { HistoryContext, HistoryProvider };