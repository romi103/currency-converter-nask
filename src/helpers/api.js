import axios from 'axios';
const path = "https://api.exchangeratesapi.io/latest"

export default {
    getCurrencyLatest() {
        return axios.get(`${path}`);
    },

    getCurrencyRates(symbolBase) {
        return axios.get(`${path}?base=${symbolBase}`);
    }
}