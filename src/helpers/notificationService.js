export default {
    displayNotification() {
        const container = document.getElementById("notification");
        const close = document.getElementById("close-notification");

        container.classList.add("show")

        function closeNotification() {
            container.classList.remove("show");
            close.removeEventListener("click", close, true);
        }


        close.addEventListener("click", closeNotification)
    }
}