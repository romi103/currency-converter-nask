const swapCurrency = (args, state, { changeValue }) => {
    changeValue(state, 'currencyFrom', () => state.lastFormState.values.currencyTo);
    changeValue(state, 'currencyTo', () => state.lastFormState.values.currencyFrom);
}

const setValue = ([field, value], state, { changeValue }) => {
    changeValue(state, field, () => value)
}


export default { swapCurrency, setValue }