import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, css } from 'aphrodite';



function Move({ move, children }) {

    const styles = StyleSheet.create({

        common: {
            transition: "all 0.4s",
        },


        moveLeft: {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            transform: "translateX(-43.5%)",
            zIndex: 1,
            transition: "all 0.4s",

        },

        moveRight: {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            transform: "translateX(43.5%)",
            zIndex: 0,
            transition: "all 0.4s",
        },
    });

    return (
        <div className={css(
            move === "left" ? styles.moveLeft : null,
            move === "right" ? styles.moveRight : null,
            styles.common
        )}>
            {children}
        </div>
    )
}

Move.typeProps = {
    move: PropTypes.oneOf(["left", "right"])
}

export default Move;