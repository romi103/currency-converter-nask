import React, { Fragment, useContext } from 'react';
import { Link, useRouteMatch } from "react-router-dom";
import { HistoryContext } from "../../helpers/historyContext";
import "./_history.scss";

export default function History(props) {
    const { conversions, setConversion } = useContext(HistoryContext);
    const clearHistory = () => (
        setConversion({
            type: "clear"
        }));

    let isHistoryRoute = useRouteMatch("/history");

    const table = () => (
        conversions.map((conversion, index) => {
            return (
                <Fragment key={index}>
                    <span style={{ textAlign: "left" }}>{conversion.date}</span>
                    <span>{conversion.currencyFromValue} {conversion.currencyFrom}</span>
                    <span>
                        <svg className="history__arrow" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                        </svg>
                    </span>
                    <span>{conversion.convertedValue} {conversion.currencyTo}</span>
                </Fragment>

            )
        })
    );


    return (
        <Fragment>
            <div className="history">
                <div className="history__container">
                    <div className="history__table-container">
                        <div className="history__header">
                            <span style={{ textAlign: "left" }}>Data</span>
                            <span>Przed konwersją</span>
                            <span></span>
                            <span>Po konwersji</span>
                        </div>
                        {
                            (conversions === undefined || conversions.length === 0) ? <p className="history__no-conversion">Brak historii konwersji walut</p> : (
                                <div className="history__table">
                                    {table()}
                                </div>
                            )
                        }
                    </div>
                    <input type="button" className={`history__clear-history ${conversions ? "history__clear-history--active" : null}`} value="Wyczyść historię" onClick={clearHistory} />
                </div>
                <div className="navigation">
                    {(isHistoryRoute) ? (
                        <Link className="navigation__nav" to="/">
                            <svg className="navigation__close" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M6 18L18 6M6 6l12 12"></path></svg>
                        </Link>) : null
                    }

                    <Link to="/history">Historia</Link>
                </div>
            </div>
        </Fragment>
    )
}
