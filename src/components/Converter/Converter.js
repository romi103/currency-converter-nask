import React, { useContext, useState } from 'react';
import { Form, Field, FormSpy } from 'react-final-form';
import "./_converter.scss";
import api from "../../helpers/api";
import { HistoryContext } from "../../helpers/historyContext";
import TextInput from "../TextInput/TextInput";
import Select from "../Select/Select";
import { required, mustBeNumber, minValue, composeValidators } from "../../helpers/validators";
import PropTypes from "prop-types"
import mutators from "../../helpers/formMutaros";
import notificationService from "../../helpers/notificationService";



function Converter({ currencyList, ...props }) {
    const { conversions, setConversion } = useContext(HistoryContext);

    const [loading, setLoading] = useState(false);
    return (
        <div className="converter">

            <Form
                onSubmit={() => null}
                mutators={mutators}
                render={({ handleSubmit, submitting, values, form, valid, ...rest }) => {
                    return <fieldset disabled={loading}>
                        <form onSubmit={handleSubmit}>
                            <h1 className="converter__heading">Konwerter walut</h1>

                            <div className="converter__value">
                                <Field name="currencyFromValue" placeholder="Wpisz kwotę" validate={composeValidators(required, mustBeNumber, minValue(0.1))} component={TextInput} currency={values.currencyFrom} />
                            </div>
                            <div className="converter__value">
                                <Field name="currencyToValue" placeholder="Wynik" component={TextInput} currency={values.currencyTo} />
                            </div>


                            <div className="converter__currency-select">
                                <Field name="currencyFrom" initialValue="PLN" options={currencyList} component={Select} />
                                <span type="button" onClick={form.mutators.swapCurrency}>
                                    <svg className="converter__currency-swap" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path></svg>
                                </span>
                                <Field name="currencyTo" initialValue="USD" options={currencyList} component={Select} />
                            </div>



                            <FormSpy>
                                {props => (
                                    <div>
                                        <button
                                            type="submit"
                                            className={`converter__convert-button ${loading ? "converter__convert-button--loading" : null}`}
                                            disabled={props.submitting || loading || (props.invalid && props.touched.currencyFrom)}
                                            onClick={() => {
                                                const form = props.form;
                                                const state = form.getState();
                                                const { currencyFrom, currencyTo, currencyFromValue } = state.values;
                                                form.submit()

                                                if (valid) {
                                                    setLoading(true);

                                                    api.getCurrencyRates(currencyFrom).then(res => {
                                                        if (res) {
                                                            setTimeout(() => {
                                                                setLoading(false)
                                                                const { rates } = res.data;
                                                                const rate = rates[currencyTo];
                                                                const convertedValue = (currencyFromValue * rate).toFixed(2);

                                                                props.form.mutators.setValue("currencyToValue", convertedValue)

                                                                setConversion({
                                                                    type: "add",
                                                                    newConversion: {
                                                                        currencyFrom,
                                                                        currencyTo,
                                                                        currencyFromValue,
                                                                        convertedValue,
                                                                        date: new Date().toLocaleDateString('pl-PL')
                                                                    }
                                                                })
                                                            }, 1000)
                                                        }
                                                    }).catch(() => {
                                                        setLoading(false);
                                                        notificationService.displayNotification();
                                                    })
                                                }
                                            }}
                                        >
                                            {loading ? "KONWERTUJE..." : "KONWERTUJ"}
                                        </button>
                                    </div>
                                )}
                            </FormSpy>
                        </form>
                    </fieldset>
                }}


            />

        </div>
    )
}

Converter.prototype = {
    currencyList: PropTypes.array.required
}

export default Converter;

