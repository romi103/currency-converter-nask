import React, { Component, Fragment } from 'react';
import {
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import Converter from '../Converter/Converter';
import Wrapper from "../Wrapper/Wrapper";
import History from "../History/History";
import { HistoryProvider } from "../../helpers/historyContext";
import api from "../../helpers/api";
import Move from "../Move";
import Error from "../Error";


export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currencyList: []
        }
    }

    componentDidMount() {
        api.getCurrencyLatest().then(res => {
            this.setState(() => ({
                currencyList: Object.keys(res.data.rates).map(key => key)
            }))
        })
    }

    render() {
        return (
            <Router>
                <Error>
                    <Wrapper>
                        <HistoryProvider>
                            <Route exact path="/history" render={routeProps => (
                                <Fragment>
                                    <Move move="left">
                                        <Converter currencyList={this.state.currencyList} />
                                    </Move>
                                    <Move move="right">
                                        <History />
                                    </Move>
                                </Fragment>
                            )}>
                            </Route>
                            <Route exact path="/" render={routeProps => (
                                <Fragment>
                                    <Move>
                                        <Converter currencyList={this.state.currencyList} />
                                    </Move>
                                    <Move>
                                        <History />
                                    </Move>
                                </Fragment>
                            )}>
                            </Route>
                        </HistoryProvider>
                    </Wrapper>
                </Error>
            </Router >
        )
    }

}