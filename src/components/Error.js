import React from "react"

class Error extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch() {
        this.setState({ hasError: true });

    }

    render() {
        if (this.state.hasError) {
            return <h1>Wystąpił błąd. Skontaktuj się z Supportem</h1>;
        }
        return this.props.children;
    }
}

export default Error;