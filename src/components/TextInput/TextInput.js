import React, { Fragment } from 'react';
import Input from '@material-ui/core/Input';
import { makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';


const useStyles = makeStyles({
  root: {
    fontSize: "2rem",

  },

  label: {
    fontSize: "1.6rem",
    color: "#C2CBD9",
  },

  error: {
    fontSize: "1.6rem",
    marginTop: "0.5rem"
  }
});

export default function InputText({
  input: { name, onChange, value },
  meta,
  currency = "",
  ...rest
}) {
  const classes = useStyles();
  return (
    <Fragment>
      <Input
        {...rest}
        name={name}
        error={meta.error && meta.touched}
        onChange={onChange}
        fullWidth={true}
        value={value}
        disableUnderline={false}
        classes={{ root: classes.root }}
        endAdornment={
          <InputAdornment position="end" classes={{ root: classes.label }} children={
            <span>{currency}</span>

          } />
        }
      />
      {meta.error && meta.touched ? <InputLabel error classes={{ root: classes.error }}>{meta.error}</InputLabel> : null}
    </Fragment>
  )
}
