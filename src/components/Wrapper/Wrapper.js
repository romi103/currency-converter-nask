import React from 'react'
import "./_wrapper.scss";
export default function Wrapper(props) {

    return (
        <div className="outer">
            {props.children}
        </div>
    )
}

