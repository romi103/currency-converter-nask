import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles({
    root: {
        fontSize: "2rem"
    },

    menu: {
        maxHeight: "15rem",
    }
});



export default function SelectInput({
    input: { name, onChange, value },
    meta,
    options = [],
    ...rest
}) {
    const classes = useStyles();

    return (
        <Select
            {...rest}
            name={name}
            error={meta.error && meta.touched}
            onChange={onChange}
            value={value}
            className={classes.root}
            MenuProps={{
                getContentAnchorEl: null,
                anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                },
                classes: {
                    paper: classes.menu
                }

            }}
        >
            {options.map((option) => {
                return <MenuItem key={option} value={option}>{option}</MenuItem>
            })}
        </Select>
    );
}
